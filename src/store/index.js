import Vue from 'vue'
import Vuex from 'vuex'
import marked from "marked"
import hive from "@hiveio/hive-js"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    postList: {},
    isLoad: false,
    postContent:{},
    postImg: '',
    postHtml: ''
  },
  mutations: {
    ADD_POST: (state, posts) => {
      state.postList= posts
      state.isLoad = true
    },
    ADD_POST_CONTENT: (state, post) => {
      state.postContent= post
      state.isLoad = true
    },
    ADD_POST_HTML: (state, html) => {
      state.postHtml= html
    },
    ADD_POST_IMG: (state, img) => {
      state.postImg= img
    }
  },
  actions: {
    async getTrendingTag({commit},value){
      const result = await new Promise(function(resolve, reject){ hive.api.getDiscussionsByTrending(
        value,
        function(err, result) {
          resolve(result)
        })
      }).then( (data) => {
        commit('ADD_POST',data) 
      })
    },
    async getPostContent({commit},value){
      const result = await new Promise(function(resolve, reject){ hive.api.getContent(value.author, value.permlink, function(err, result) {
        resolve(result)
        })
      }).then( (data) => {
        commit('ADD_POST_CONTENT',data)
        const renderer = new marked.Renderer()
      if(data.body !== undefined) {
        var html = marked(data.body, {
            breaks: true,
            headerIds: false,
            highlight(code) {
            return 0
            },
            renderer: renderer
        })
        var imgRegex = new RegExp("<img[^>]*?>", "g")
        let tagImg = html.match(imgRegex)
        html = html.replace(html.match(imgRegex), "")
        commit('ADD_POST_HTML',html)
        commit('ADD_POST_IMG',tagImg)
        } 
      })
    }
  },
  getters: {
    isLoad: state => state.isLoad,
    postList: state => state.postList,
    postContent: state => state.postContent,
    postHtml: state => state.postHtml,
    postImg: state => state.postImg,
    
  }
})
