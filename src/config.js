

const r = require('rethinkdb');
let config = {host: '64.225.23.6', port: 32769};
let connect = null;


r.connect( config, function(err, conn) {
    if (err) throw err;
    connection = conn;
})


r.db('test').tableCreate('authors').run(connection, function(err, result) {
    if (err) throw err;
    console.log(JSON.stringify(result, null, 2));
})


module.exports = {
	rethinkdb: config
};
