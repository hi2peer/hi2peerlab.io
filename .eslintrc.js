module.exports = {
    root: true,
    env: {
      node: true
    },
    extends: ['plugin:vue/strongly-recommended'],
    plugins: ['vue'],
    rules: {
      'vue/no-parsing-error': [2, { 'x-invalid-end-tag': false }],
      'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      semi: ['error', 'never']
    },
    parserOptions: {
      parser: 'babel-eslint'
    }
  }
  
